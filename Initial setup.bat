@echo off
cls

git update-index --assume-unchanged "deploy_key"
git update-index --assume-unchanged "deploy_key.pub"
git update-index --assume-unchanged "_bericht.pdf"
git update-index --assume-unchanged "_Formalien.pdf"
git update-index --assume-unchanged "_topic.pdf"

@echo on

ssh-keygen -f deploy_key -N ""

Echo Now follow the following guide:

Echo 

Echo https://marcosschroh.github.io/posts/autobumping-with-gitlab/

pause