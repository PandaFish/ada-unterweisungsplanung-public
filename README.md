# AdA-Unterweisungsplanung
Repository für die AdA Unterweisungsplanung an der DHBW
- Dieses Repository wurde angepasst, so dass vertrauliche Informationen (Bspw. Matrikelnummer) hier nicht mehr enthalten sind. Es weicht minimal von der eigentlichen Abgabe ab und soll zur Orientierung von Studenten / Angehenden Ausbildern nach mir dienen.
- Feedback ist erwünscht. Kontaktmöglichkeiten gemäß [Profile Repository](https://gitlab.com/PandaFish/pandafish)

## Preview
Eine Vorcompilierte PDF-Version des Templates kann hier eingesehen werden: [_unterweisungsplanung.pdf](https://dhbw-karlsruhe1.gitlab.io/ada-unterweisungsplanung-public/_unterweisungsplanung.pdf)

## Empfohlene TeX Editoren:
- [Visual Studio Code](https://code.visualstudio.com/download)
- [Notepad++](https://notepad-plus-plus.org/downloads/)

## Empfohlener TeX Compiler für dieses Projekt:
[TeXLive](https://www.tug.org/texlive/)

## Build / Compile to PDF:
Installiere einen TeX Compiler [z.B. TeXLive](#empfohlener-tex-compiler-f%C3%BCr-dieses-projekt)

**Auf Windows:** Starte das [CompileTeX.bat](Tex/Compiler/CompileTeX.bat) script und lass es bis zum Ende durch laufen.<br/>
 -> *Alternativ*: für eine periodisch wiederholende automatische Compilation: Starte [Compile Scheduler.bat](Tex/Compile%20Scheduler.bat) - Dieses Batch skript kann zu jedem Zeitpunkt beliebig gekillt werden

**Auf Linux:** Starte das [CompileTeX.sh](Tex/Compiler/CompileTeX.sh) script und lass es bis zum Ende durch laufen.

Die **Ausgabe** sollte nach einem erfolgreichen build im Repository-Root Verzeichnis unter dem Name [_unterweisungsplanung.pdf](_unterweisungsplanung.pdf) landen.


## Some Required TeX Packages:
[Listed here](\TeX\meta\TeXLibs.md)<br/>
Falls Miktex installiert ist, sollte dies Automatisch verwaltet werden.
