title Biblatex auto-compiler
cd ..
:looper
    xcopy "TeX" "build" /s /e /y >>build/.move.log
    cd build
    pdflatex unterweisungsplanung.
    bibtex unterweisungsplanung.aux
    cd ..
    START /min Compiler/CleanUp.bat
    TIMEOUT /t 100
goto looper
exit
    