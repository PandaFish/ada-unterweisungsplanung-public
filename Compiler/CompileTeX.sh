#!/bin/bash

function setup(){
    cd ..
    pwd
    mkdir build
    echo Setup done.
}

function copyToBuild(){
    echo Cleaning build/...
    rm -r build
    mkdir build
    echo Copying to build/
    echo pwd
    pwd
    cp -r ./TeX/* ./build
}

function compile(){
    echo Compiling...
    sleep 1
    echo "P"
    pdflatex _unterweisungsplanung
    
    sleep 1
    echo "B"
    bibtex _unterweisungsplanung
    
    sleep 1
    echo "P"
    pdflatex _unterweisungsplanung
    
    sleep 1
    echo "P"
    pdflatex _unterweisungsplanung
    
    sleep 1
    echo " x"
    pdflatex _Formalien
    
    sleep 1
    echo -
    echo Compilation done.
}

function movePDF(){
    sleep 1
    mv ./*.pdf ../
    echo Moved pdf to ../
}

function cleanUp(){
    echo Cleaning up...
    del_ext "*.aux" 
    del_ext "*.bbl"
    del_ext "*.blg"
    del_ext "*.fdb_latexmk"
    del_ext "*.fls"
    del_ext "*.for"
    del_ext "*.idx"
    del_ext "*.ilg"
    del_ext "*.ind"
    del_ext "*.lof"
    del_ext "*.lol"
    del_ext "*.lot"
    del_ext "*.synctex.gz"
    del_ext "*.toc"
    del_ext "*-blx.bib"
    del_ext "*.run.xml"
    
    del_ext "unterweisungsplanung.log"
    sleep 1
    echo Cleanup done.
}

function del_ext(){
 local del_ext="${1}"
 rm $del_ext
}

echo pwd
pwd
setup
copyToBuild
cd "build/"
echo ls
ls
echo pwd
pwd
compile
movePDF
cleanUp
cd ..
echo pwd
pwd