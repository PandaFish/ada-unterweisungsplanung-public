call :setup
cls
call :CleanUp
cd ..
exit

:setup
    @Echo off
    cls
    Echo Setup done.
    cd ..
    cd TeX
goto :eof

:del_ext
 set del_ext=%1
 del /f /q /s "%del_ext%"
goto :eof